using Bogus;
using Bogus.Extensions.Brazil;
using Microsoft.VisualStudio.TestPlatform.Utilities;
using Prova.Domain.Entities;
using Xunit.Abstractions;
using static Bogus.DataSets.Name;

namespace Prova.Domain.UnitTests.Entities.Products
{
    public class ProductTests : IClassFixture<ProductTestsFixture>
    {
        private readonly ProductTestsFixture _fixture;
        private readonly ITestOutputHelper _outputHelper;

        public ProductTests(ProductTestsFixture fixture, ITestOutputHelper outputHelper) 
        {
            _fixture = fixture;
            _outputHelper = outputHelper;
        }
        [Fact]
        public void SetName_WhenNameIsWhiteSpace_ShouldThrow()
        {
            var product = _fixture.GenerateValidProduct();
            var newName = "  ";

            var ex = Record.Exception(() => product.SetName(newName));

            Assert.Equal("The Name cannot be empty", ex.Message);
            _outputHelper.WriteLine(ex.Message);
        }

        [Fact]
        public void SetPrice_WhenPriceIsZero_ShouldFalse()
        {
            var product = (Product)new Faker<Product>("pt_BR")
                    .CustomInstantiator(f => new Product(
                        f.Commerce.Product(),
                        f.Random.Decimal()));

            var newPrice = 0M;

            var result = product.SetPrice(newPrice);

            Assert.False(result);
        }
    }
}