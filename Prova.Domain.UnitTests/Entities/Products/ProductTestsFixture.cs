﻿using Prova.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prova.Domain.UnitTests.Entities.Products
{
    public class ProductTestsFixture
    {
        public Product GenerateValidProduct()
        {
            return new Product("Meat", 7.5M);
        }
    }
}
