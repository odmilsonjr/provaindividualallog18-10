﻿namespace Prova.Domain.Entities
{
    public class Product
    {
        public Guid Id { get; init; }

        public string Name { get; private set; }

        public decimal Price { get; private set; }

        public Product(string name, decimal price)
        {
            Id = Guid.NewGuid();
            Name = name;
            Price = price;
        }

        public void SetName(string name)
        {
            if (String.IsNullOrWhiteSpace(name))
            {
                throw new Exception("The Name cannot be empty");
            }

            Name = name;
        }

        public bool SetPrice(decimal price)
        {
            if (price <= 0)
            {
                return false;
            }

            Price = price;
            return true;
        }
    }
}